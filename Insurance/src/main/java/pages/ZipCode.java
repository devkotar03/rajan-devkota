package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ZipCode {
	static WebDriver driver;
	WebElement element;
	By ZipCode = By.xpath("//input[@name='ZipCode']");
    By getquote =By.xpath("//*[@id='qsButton_overlay']");

	public ZipCode(WebDriver driver) {

		this.driver = driver;

	}

	public void EnterZipCode(String Zip) {
		driver.findElement(ZipCode).sendKeys(Zip);

	}
	public void getquote() {
		driver.findElement(getquote).click();
	
}
}