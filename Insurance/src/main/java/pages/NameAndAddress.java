package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NameAndAddress {
	 WebDriver driver;
	WebElement element;
	By FName = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_FirstName']");
	By MName = By.xpath("//*[@id='NameAndAddressEdit_embedded_questions_list_MiddleInitial']");
	By LNmae = By.xpath("//*[@id='NameAndAddressEdit_embedded_questions_list_LastName']");
	By DOB = By.xpath("//*[@id='NameAndAddressEdit_embedded_questions_list_DateOfBirth']");
	By StreetAddress = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_MailingAddress']");
	
	By APT = By.xpath("//*[@id='NameAndAddressEdit_embedded_questions_list_ApartmentUnit']");
	By StartQuote = By.xpath("//*[@class='blue']");

	public NameAndAddress(WebDriver driver) {

		this.driver = driver;

	}

	public void firstname(String name) {
		driver.findElement(FName).sendKeys(name);

	}

	public void lastname(String name) {
		driver.findElement(LNmae).sendKeys(name);

	}

	public void dateofbirth(String date) {
		driver.findElement(DOB).sendKeys(date);

	}

	public void Address(String address) throws InterruptedException {
		driver.findElement(DOB).sendKeys(Keys.TAB);
		Thread.sleep(2000);
		driver.findElement(StreetAddress).sendKeys(address);
		Thread.sleep(2000);
	}

	public void Apartment(String apt) {
		driver.findElement(APT).sendKeys(apt);

	}

	public void GetaQuote() {
		driver.findElement(StartQuote).click();

	}
}