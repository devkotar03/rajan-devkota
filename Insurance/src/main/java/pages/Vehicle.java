package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ISelect;
import org.openqa.selenium.support.ui.Select;

public class Vehicle {
	WebDriver driver;
	WebElement element;
	By Year = By.xpath("//li[contains(text(),'2013')]");
	By Name = By.xpath("//li[contains(text(),'Nissan')]");
	By Model = By.xpath("//li[contains(text(),'Altima')]");
	By Body = By.xpath("//select[@id='VehiclesNew_embedded_questions_list_BodyStyle']");
	By primaryuse = By.xpath("//select[@id='VehiclesNew_embedded_questions_list_VehicleUse']");
	By ownorlease = By.xpath("//select[@id='VehiclesNew_embedded_questions_list_OwnOrLease']");
	By Buyyears = By.xpath("//select[@id='VehiclesNew_embedded_questions_list_LengthOfOwnership']");
	By Assistantsystem = By.xpath("//input[@id='VehiclesNew_embedded_questions_list_BlindSpotWarning_N']");
	By Continue = By.xpath("//button[contains(text(),'Continue')]");
	
	public Vehicle(WebDriver driver) {

		this.driver = driver;

	}

	public void Vehicleyear() {
		driver.findElement(Year).click();

	}

	public void vehiclename() {
		driver.findElement(Name).click();

	}

	public void vehiclemodel() {
		driver.findElement(Model).click();

	}

	public void vehiclebody(String bodytype) {
		WebElement element = driver.findElement(Body);
		Select Body = new Select(element);
		Body.selectByVisibleText(bodytype);

	}

	public void Vehicleuse(String use ) throws InterruptedException {
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(primaryuse).click();
		Select item = new Select (driver.findElement(primaryuse));
		 item.selectByVisibleText(use);
	}
	

	public void vehicleownlease(String ownlease) throws InterruptedException {
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(ownorlease).sendKeys(ownlease);

	}

	public void Vehiclebuyyear(String year ) {
		driver.findElement(Buyyears).click();
		Select item = new Select (driver.findElement(Buyyears));
		 item.selectByIndex(3);

	}

	public void Vehicleassistant() throws InterruptedException {
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(Assistantsystem).click();

	}

	public void Continue() {
		driver.findElement(Continue).click();
	}
}