package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Homepage {
	WebDriver driver;
	WebElement element;
	By autoquote = By.xpath("(//p[@class='txt'])[1]");

	public Homepage(WebDriver driver) {

		this.driver = driver;

	}

	public void Progressiveauto() {
		driver.findElement(autoquote).click();

	}
}
