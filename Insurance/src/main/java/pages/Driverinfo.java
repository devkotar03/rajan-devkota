package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Driverinfo {
	WebDriver driver;
	WebElement element;
	By Gender = By.xpath("//input[@id='DriversAddPniDetails_embedded_questions_list_Gender_M']");
	By Maritialstatus = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_MaritalStatus']");
	By Education = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation']");
	By Employment = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_EmploymentStatus']");
	By Residence = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_PrimaryResidence']");
	By Movein = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_HasPriorAddress']");
	By License = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_LicenseStatus']");
	By Year = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_DriverYearsLicensed']");
	By Accident = By.xpath("//input[@id='DriversAddPniDetails_embedded_questions_list_HasAccidentsOrClaims_N']");
	By Voilation =By.xpath("//input[@id='DriversAddPniDetails_embedded_questions_list_HasTicketsOrViolations_N']");
	By Continue = By.xpath("//button[contains(text(),'Continue')]");

	public Driverinfo(WebDriver driver) {

		this.driver = driver;

	}

	public void Gender() throws InterruptedException {
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(Gender).click();

	}

	public void Maritual(String status) {
		driver.findElement(Maritialstatus).sendKeys(status);

	}

	public void Education(String degree) {
		driver.findElement(Education).click();
		Select item = new Select(driver.findElement(Education));
		item.selectByIndex(6);

	}

	public void Employment(String status) {
		driver.findElement(Employment).click();
		Select item = new Select(driver.findElement(Employment));
		item.selectByIndex(5);

	}

	public void Residence(String residence) throws InterruptedException {
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(Residence).sendKeys(residence);

	}

	public void Moved(String status) {
		driver.findElement(Movein).sendKeys(status);
	}

	public void Drivinglicense(String license) {
		driver.findElement(License).click();
		Select item = new Select(driver.findElement(License));
		item.selectByVisibleText(license);
	}

	public void Yearslicensed(String year) {
		driver.findElement(Year).click();
		Select item = new Select(driver.findElement(Year));
		item.selectByVisibleText(year);
	}

	public void Accidents() {
		driver.findElement(Accident).click();

	}

	public void Voilation() {
		driver.findElement(Voilation).click();

	}
	public void Continue() {
		driver.findElement(Continue).click();
	}
}