package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.Driverinfo;
import pages.Homepage;
import pages.NameAndAddress;
import pages.Vehicle;
import pages.ZipCode;

public class ProgressiveTest {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		invokeBrowser();
		performActions();
		performActions1();
		performActions2();
		performActions3();
		performActions4();
	}
	
	public static void invokeBrowser() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "//Users//rajandevkota//Desktop//Driver//chromedriver");
		driver = new ChromeDriver();
		Thread.sleep(1000);
		driver.get("http://www.progressive.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if (driver.getTitle().contains("Progressive")) {
			System.out.println(driver.getTitle() + "......................was launched");
		} else {
			System.exit(0);
		}

	}

	public static void performActions() {
		Homepage obj = new Homepage(driver);
		obj.Progressiveauto();
	}

	public static void performActions1() {
		ZipCode obj = new ZipCode(driver);
		obj.EnterZipCode("75038");
		obj.getquote();
	}

	public static void performActions2() throws InterruptedException {
		NameAndAddress obj = new NameAndAddress(driver);
		obj.firstname("Rajan");
		obj.lastname("Devkota");
		obj.dateofbirth("07/02/1995");
		obj.Address("3828 portland street");
		obj.Apartment("329");
		obj.GetaQuote();
	}

	public static void performActions3() throws InterruptedException {
		Vehicle obj = new Vehicle(driver);
		obj.Vehicleyear();
		obj.vehiclename();
		obj.vehiclemodel();
		obj.vehiclebody("4DR 4CYL");
		Thread.sleep(1000);
		obj.Vehicleuse("Personal (to/from work or school, errands, pleasure)");
		obj.vehicleownlease("Own");
		obj.Vehiclebuyyear("3 years - 5 years");
		Thread.sleep(1000);
		obj.Vehicleassistant();
		obj.Continue();

	}

	public static void performActions4() throws InterruptedException {
		Driverinfo obj = new Driverinfo(driver);
		obj.Gender();
		obj.Maritual("Single");
		obj.Education("College degree");
		obj.Employment("Student(full time");
		obj.Residence("Rent");
		obj.Moved("No");
		obj.Drivinglicense("Valid");
		obj.Yearslicensed("3 years or more");
		obj.Accidents();
		obj.Voilation();
		obj.Continue();
	}

    public void tearDownTest() {
    	driver.close();
    	driver.quit();
    	System.out.println("Test Completed Successfully");
    	
    }

}
