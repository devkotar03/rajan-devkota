package Testng;

import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.Driverinfo;
import pages.Homepage;
import pages.NameAndAddress;
import pages.Vehicle;
import pages.ZipCode;

public class Progressivetestng {
	public static WebDriver driver;

	@BeforeSuite
	public void setup() {

	}

	@BeforeTest
	public void setupTest() {
		System.setProperty("webdriver.chrome.driver", "//Users//rajandevkota//Desktop//Driver//chromedriver");
		driver = new ChromeDriver();
	}

	public static void navigateToWebPage() {
		driver.get("http://www.progressive.com");
		if (driver.getTitle().contains("Progressive")) {
			System.out.println(driver.getTitle() + "......................was launched");
		} else {
			System.exit(0);
		}
	}

	@Test(priority = 0)
	public static void performActions() {
		navigateToWebPage();
		Homepage obj = new Homepage(driver);
		obj.Progressiveauto();
	}

	@Test(priority = 1)
	public static void performActions1() throws InterruptedException {
		ZipCode obj = new ZipCode(driver);
		TimeUnit.SECONDS.sleep(2);
		obj.EnterZipCode("75038");
		obj.getquote();
	}

	@Test(priority = 2)
	public static void performActions2() throws InterruptedException {
		NameAndAddress obj = new NameAndAddress(driver);
		TimeUnit.SECONDS.sleep(5);
		obj.firstname("Rajan");
		obj.lastname("Devkota");
		obj.dateofbirth("07/02/1995");
		obj.Address("3828 portland street");
		obj.Apartment("329");
		obj.GetaQuote();
	}

	@Test(priority = 3)
	public static void performActions3() throws InterruptedException {
		Vehicle obj = new Vehicle(driver);
		TimeUnit.SECONDS.sleep(5);
		obj.Vehicleyear();
		TimeUnit.SECONDS.sleep(5);
		obj.vehiclename();
		TimeUnit.SECONDS.sleep(5);
		obj.vehiclemodel();
		TimeUnit.SECONDS.sleep(5);
		obj.vehiclebody("4DR 4CYL");
		Thread.sleep(1000);
		obj.Vehicleuse("Personal (to/from work or school, errands, pleasure)");
		obj.vehicleownlease("Own");
		obj.Vehiclebuyyear("3 years - 5 years");
		Thread.sleep(1000);
		obj.Vehicleassistant();
		obj.Continue();

	}

	@Test(priority = 4)
	public static void performActions4() throws InterruptedException {
		Driverinfo obj = new Driverinfo(driver);
		TimeUnit.SECONDS.sleep(1);
		obj.Gender();
		TimeUnit.SECONDS.sleep(1);
		obj.Maritual("Single");
		TimeUnit.SECONDS.sleep(1);
		obj.Education("College degree");
		TimeUnit.SECONDS.sleep(1);
		obj.Employment("Student(full time");
		obj.Residence("Rent");
		obj.Moved("No");
		obj.Drivinglicense("Valid");
		obj.Yearslicensed("3 years or more");
		obj.Accidents();
		obj.Voilation();
		obj.Continue();
	}

	@AfterTest
	public void tearDownTest() {
		driver.close();
		driver.quit();
		System.out.println("Test Completed Successfully");

	}

	@AfterSuite
	public void tearDown() {

	}

}
